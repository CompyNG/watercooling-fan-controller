Watercooling Fan Controller
===========================

A simple fan controller for a water-cooled PC.

Targets an ESP8266, with a web page to show temperature and fan RPM.

Uses a [Barrow
TCWD-V1](http://www.barrowint.com/product/jtl/BARROW_Fitting/cwxjt/1282.html)
for the temperature sensor.

Schematic maybe coming at some point. Or look at the code and figure out the
pinout.
