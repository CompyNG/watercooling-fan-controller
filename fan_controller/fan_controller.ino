#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266mDNS.h>
#include <variant>
#include "local_wifi.h"

#define THERMISTOR A0

unsigned long last_updated_millis = 0;

class Fan {
public:
  virtual void interrupt() = 0;
  virtual void setup() = 0;
  virtual void setPwm(unsigned speed_percent) = 0;
  virtual double getRpm() = 0;
  virtual void readTach() = 0;
  virtual double getRpmCached() = 0;
};

IRAM_ATTR void interrupt(void* ptr) {
  static_cast<Fan*>(ptr)->interrupt();
}

template<uint8_t PWM, uint8_t TACH>
class PwmFan : public Fan {
public:
  int pulseCount;
  unsigned long last_tach_read;
  double rpm_cache;

  void interrupt() override {
    pulseCount += 1;
  }

  PwmFan() { pulseCount = 0; last_tach_read = 0; }

  void setup() override {
    last_tach_read = millis();
    pinMode(PWM, OUTPUT);
    pinMode(TACH, INPUT_PULLUP);
    attachInterruptArg(digitalPinToInterrupt(TACH), ::interrupt, this, RISING);

    analogWriteFreq(25000);
    setPwm(100);
  }

  void setPwm(unsigned speed_percent) override {
    analogWrite(PWM, (speed_percent * 255 ) / 100);
  }

  double getRpm() override {
    const unsigned long current_time = millis();
    const unsigned revolutions = pulseCount / 2;
    const auto time_ms = current_time - last_tach_read;
    const auto time_s = time_ms / 1000.0;
    const auto time_m = time_s / 60.0;
    const double rpm = revolutions / time_m;
    pulseCount = 0;
    last_tach_read = millis();
    return rpm;
  }

  void readTach() override {
    rpm_cache = getRpm();
  }

  double getRpmCached() override {
    return rpm_cache;
  }
};

//   PWM, TACH
PwmFan<0, 5> fan1;
PwmFan<2, 4> fan2;
PwmFan<12, 14> fan3;
std::array<Fan*, 3> fans {&fan1, &fan2, &fan3};

float getTemperature() {
  const float R1 = 10000;
  const float c1 = 0.8797829115e-3, c2 = 2.543439899e-4, c3 = 1.684833115e-7;

  int Vo = analogRead(THERMISTOR);
  // float R2 = R1 * (1023 / (float)Vo - 1.0);
  float R2 = ((float)Vo * R1) / (1023 - (float)Vo);
  float logR2 = log(R2);
  float T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  return T - 273.15;
}

class PwmController {
public:
  uint8_t thermistor_pin;
  unsigned long last_updated_time;
  float last_used_temperature;
  unsigned last_speed_percent;

  PwmController(uint8_t thermistor_pin) :
    thermistor_pin(thermistor_pin)
  {}

  void setup() {
    last_updated_time = millis();
    last_used_temperature = 0;
  }

  template<size_t N>
  void setNewPwm(const std::array<Fan*, N>& fans, unsigned speed_percent) {
    last_speed_percent = speed_percent;
    last_updated_time = millis();

    for (const auto& fan : fans) {
      fan->setPwm(speed_percent);
    }
  }

  template<int X1, int X2, int Y1, int Y2>
  float calculatePoint(float x) {
    constexpr float slope = static_cast<float>(Y2 - Y1) / static_cast<float>(X2 - X1);
    return (slope * (x - X1)) + Y1;
  }

  template<size_t N>
  void updateFans(const std::array<Fan*, N>& fans) {
    float new_temperature = getTemperature();

    if (new_temperature >= 58) {
      last_used_temperature = new_temperature;
      setNewPwm(fans, 100);
    }

    auto time_since_last_update = millis() - last_updated_time;
    if (last_speed_percent == 100) {
      // If we're ramping down from the 'emergency' temperature, wait a little longer
      // TODO: Maybe have a time constant on this as well?
      if (new_temperature > 56) {
        return;
      }
    }

    // auto temperature_change = new_temperature - last_used_temperature;

    last_used_temperature = new_temperature;
    if (new_temperature < 30) {
      setNewPwm(fans, 0);
    } else if (new_temperature < 45) {
      setNewPwm(fans, calculatePoint<30, 40, 0, 40>(new_temperature));
    } else if (new_temperature < 50) {
      setNewPwm(fans, calculatePoint<40, 50, 40, 70>(new_temperature));
    } else if (new_temperature < 58) {
      setNewPwm(fans, calculatePoint<50, 58, 70, 100>(new_temperature));
    } else {
      setNewPwm(fans, 100);
    }
  }
};

PwmController pwm(THERMISTOR);
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

struct FanModePwm {};
struct FanModeTurbo {
  unsigned long start;
  unsigned long time;
};

std::variant<FanModePwm, FanModeTurbo> fanMode{FanModePwm{}};

void handleRoot() {
  server.chunkedResponseModeStart(200, F("text/plain"));
  char s[32];
  auto size = snprintf_P(s, sizeof(s), PSTR("Fan speed: %d%%\n"), pwm.last_speed_percent);
  server.sendContent(s, size);

  for (unsigned i = 0; i < fans.size(); ++i) {
    char s[20];
    auto size = snprintf_P(s, sizeof(s), PSTR("Fan %d: %.0f RPM\n"), i, fans[i]->getRpmCached());
    server.sendContent(s, size);
  }

  size = snprintf_P(s, sizeof(s), PSTR("Temperature: %.1f C\n"), pwm.last_used_temperature);
  server.sendContent(s, size);

  server.chunkedResponseFinalize();
}

void handleTurbo() {
  fanMode = FanModeTurbo{millis(), 10 * 1000};
  server.send(200, "text/plain", "OK");
}

void handleRest() {
  char s[128];
  auto pos = snprintf_P(s, sizeof(s), PSTR("{\"fan_speed\": %d, "), pwm.last_speed_percent);

  for (unsigned i = 0; i < fans.size(); ++i) {
    pos += snprintf_P(s+pos, sizeof(s)-pos, PSTR("\"fan_%d\": %.0f, "), i, fans[i]->getRpmCached());
  }

  pos += snprintf_P(s+pos, sizeof(s)-pos, PSTR("\"temperature_C\": %.01f}\n"), pwm.last_used_temperature);

  server.send(200, "application/json", s);
}

void setup() {
  Serial.begin(115200);

  for (const auto& fan : fans) {
    fan->setup();
  }

  pwm.setup();

  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  MDNS.begin("fan-controller");
  server.on("/", handleRoot);
  server.on("/turbo", handleTurbo);
  server.on("/data.json", handleRest);
  httpUpdater.setup(&server);
  server.begin();
  MDNS.addService("http", "tcp", 80);
}

String input = "";
int pulses = 0;

unsigned long previous_millis = 0;

void updateFans(const struct FanModePwm& fan_mode) {
  pwm.updateFans(fans);
}

void updateFans(const struct FanModeTurbo& fan_mode) {
  unsigned long current_millis = millis();
  if ((current_millis - fan_mode.start) > fan_mode.time) {
    fanMode = FanModePwm{};
  }

  for (const auto& fan : fans) {
      fan->setPwm(100);
  }
}

void loop() {
  // put your main code here, to run repeatedly:

  unsigned long current_millis = millis();
  if ((current_millis - previous_millis) > 500) {
    std::visit([](const auto& m) { return updateFans(m); }, fanMode);
    //pwm.updateFans(fans);

    for (const auto& fan : fans) {
      fan->readTach();
    }

    previous_millis = current_millis;
  }

  server.handleClient();
  MDNS.update();
}